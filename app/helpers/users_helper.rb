module UsersHelper
    
    def gravatar_for(user, class_name, option={})
      gravatar_email = user.email.chomp.downcase
      gravatar_id = Digest::MD5.hexdigest(gravatar_email)
      size = option[:size]
      gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
      image_tag(gravatar_url, alt: user.name, class: class_name)
    end
end
