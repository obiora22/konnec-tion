module ApplicationHelper
    def page_title(title ="")
        base_title = 'Konnec-tion'
        if title.blank?
            base_title
        else
            title = base_title + ' | ' + title
        end
    end
end
