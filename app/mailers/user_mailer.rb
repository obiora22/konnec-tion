class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation(user)
    @user = user
    # if Rails.env.development? || Rails.env.production?
    #   api_key = ENV['API']
    #   domain = ENV['DOMAIN']
    #   mg_client = Mailgun::Client.new(api_key)
    #   message_params = {from: 'noreply@example.com', to: @user.email, subject: 'Account activation', text: 'Hello!'}
    #   mg_client.send_message(domain, message_params)
    # else
       mail to: @user.email, subject: 'Account activation'
    # end
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user 
    # if Rails.env.development? || Rails.env.production?
    #   api_key = ENV['API']
    #   domain = ENV['DOMAIN']
    #   mg_client = Mailgun::Client.new(api_key)
    #   message_params = {from: 'noreply@example.com', to: @user.email, subject: 'Password reset', text: 'Hello'}
    #   mg_client.send_message(domain, message_params)
    # else
       mail to: @user.email, subject: 'Password reset'
    # end
   
  end
end
