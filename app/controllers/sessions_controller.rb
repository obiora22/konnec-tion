class SessionsController < ApplicationController
  def new
  end
  
  def create 
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in(user)
        params[:session][:remember_me] == '1' ? remember_persistent_session(user) : forget_persistent_session(user)
        flash[:notice] = "You've logged in succsessfully!"
        redirect_back_or(user)
      else
        message = "Account not activated. "
        message+= "Please check your email for the activation link."
        flash[:warning] = message 
        redirect_to root_url 
      end
    else 
      flash[:danger] = "Incorrect email/password combination."
      render 'new'
    end
  end
  
  def destroy 
    log_out if logged_in?
    flash[:notice] = "You've been succsessfully logged out."
    redirect_to root_url
  end
end
