class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  include SessionsHelper
  
  private 
  
   # Verifies a user is logged in 
  def require_login
    unless logged_in?
      store_location
      flash[:alert] = "You have to be logged in."
      redirect_to login_url 
    end
  end
  
end
