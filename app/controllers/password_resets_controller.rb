class PasswordResetsController < ApplicationController
  before_action :find_user, only: [:edit,:update]
  before_action :valid_user, only: [:edit,:update]
  before_action :check_expiration, only: [:update]
  def new
  end
  
  def create 
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user 
      @user.create_password_reset_digest 
      #@user.send_password_reset_email
      UserMailer.password_reset(@user).deliver_now 
      flash[:notice] = 'Email sent with password reset link'
      redirect_to root_url 
    else 
      flash[:warning] = 'Email address entered not found'
      render 'new'
    end
  end
  
  def edit
    
  end
  
  def update 
    if params[:user][:password].empty?
      @user.errors.add(:password,"can't be empty.")
      render 'edit'
    elsif @user.update_attributes(user_params) 
      log_in(@user)
      flash[:success] = 'Password has been reset.'
      redirect_to @user 
    else 
      render 'edit'
    end
    #render text: 'Hello'
  end
  
  private 
  
  def user_params 
    params.require(:user).permit(:password, :password_confirmation)
  end
  def find_user 
    @user = User.find_by(email: params[:email])
  end
  
  # Confirms user 
  def valid_user 
  
    unless @user && @user.activated? && @user.authenticated?(:password_reset,params[:id])
      redirect_to root_url 
    end
  end
  
  # Check if password_reset_token has expired 
  def check_expiration 
    if @user.password_reset_token_expired? 
      flash[:warning] = 'Password reset token has expired.'
      redirect_to new_password_reset_url 
    end
  end
end
