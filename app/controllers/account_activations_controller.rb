class AccountActivationsController < ApplicationController
    
    def edit 
       user = User.find_by(email: params[:email])
  
       if user && !user.activated? && user.authenticated?(:activation,params[:id])
           user.activate_user 
           log_in(user)
           flash[:notice] = 'Your account has been activated.'
           redirect_to user_path(user)
       else
           flash[:alert] = 'Your activation link is invalid.'
           redirect_to root_url
       end
    end
end
