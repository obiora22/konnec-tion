class UsersController < ApplicationController
  before_action :require_login, only: [:edit,:update,:index, :followers,:following]
  before_action :correct_user, only: [:edit, :update]
  before_action :check_if_user_is_admin, only: :destroy
  def new 
    @user = User.new 
  end
  
  def index 
   # @users = User.all # returns  `ActiveRecord::Relation` which is lazy evaluated 
   @users = User.paginate(page: params[:page], per_page: 25)
  end
  
  def show 
     @user = User.find(params[:id])
     @posts = @user.posts.paginate(page: params[:page], per_page: 25)
  end
  
  def create 
    @user = User.new(user_params)
    if @user.save
      #log_in(@user)
      #flash[:notice] = 'Welcome to Connec-tion!'
      #redirect_to @user
      UserMailer.account_activation(@user).deliver_now
      #@user.send_activation_email
      flash[:notice] = "Please check your email for to activate your account."
      redirect_to root_url 
    else
      flash.now[:error] = 'Something went wrong!'
      render 'new'
    end
  end
  
  def edit
   
  end
  
  def update 
    if @user.update_attributes(user_params)
      flash[:success] = 'Your profile has been updated.'
      redirect_to @user 
    else 
      flash[:error] = 'Something went wrong!'
      render 'edit'
    end
  end
  
  def destroy 
    @user = User.find(params[:id])
    @user.destroy 
    flash[:success] = 'User has been deleted.'
    redirect_to users_url 
  end
  
  # Returns followers 
  def followers 
   @title = 'Followers'
   @user = User.find(params[:id])
   @users = @user.followers.paginate(page: params[:page])
   render 'show_follow'
  end
  
  # Returns following 
  def following 
    @title = 'Following' 
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end
  private 
  
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
  
  
  
  # Filters
  
  # Verifies a user is logged in 
  # def require_login
  #   unless logged_in?
  #     store_location
  #     flash[:alert] = "You have to be logged in."
  #     redirect_to login_url 
  #   end
  # end
  
  # Checks identity of loggged in user.
  #  If `true`, sets `@user`.
  def correct_user 
    @user = User.find(params[:id])
    redirect_to root_url unless current_user?(@user)
  end
  
  # Checks if current_user has admin privileges 
  
  def check_if_user_is_admin 
   redirect_to root_url unless current_user.admin?
  end
end
