class User < ActiveRecord::Base
    attr_accessor :remember_token , :activation_token , :password_reset_token 
    before_save :email_downcase
    before_create :create_activation_code
    has_secure_password 
    has_many :posts , dependent: :destroy 
    
    validates :name, presence: true 
    validates :email, presence: true 
    validates :name, length: {maximum: 40}
    email_regex = /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
    validates :email, length: {maximum: 255}, uniqueness: { case_sensitive: false }
    validates :email, format: {with: email_regex, message: 'format is wrong.'}
    validates :password, length: {minimum: 6}, allow_nil: true 
    
    # user-relationship association 
    has_many :active_relationships, class_name: 'Relationship', foreign_key: 'follower_id', dependent: :destroy 
    has_many :following, :through => :active_relationships, source: 'followed'
    
    has_many :passive_relationships, class_name: 'Relationship', foreign_key: 'followed_id', dependent: :destroy 
    has_many :followers, :through => :passive_relationships, source: 'follower'
    
    # follows a user 
    def follow(other_user)
      self.active_relationships.create(followed_id: other_user.id)
    end
    # unfollows a user 
    def unfollow(other_user)
      self.active_relationships.find_by(followed_id: other_user.id).destroy 
    end
    # returns true if user is being followed
    def follows?(other_user)
      self.following.include?(other_user)
    end
    # posts feed for user home page 
    #TODO Look up ways to pull post feeds using a background job.
    def feed 
      # Perform an SQL subquery on the database level to avoid pulling array of `folllowing_ids` into memory.
      following_ids = "SELECT followed_id FROM relationships WHERE follower_id = :user_id "
      Post.where("user_id IN (#{following_ids}) OR user_id = :user_id",:user_id => id)
    end
    # Generate random token 
    def User.new_token 
       SecureRandom.urlsafe_base64  
    end
    
    # Digests random token with BCrypt.
    def User.digest(unhashed_string)
      # min_cost set to false in source code
      # Hence, `cost` will be set to 10
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost 
      # This will create a digest
      BCrypt::Password.create(unhashed_string,cost: cost)
    end
    
    # Updates user's `remember_digest` attribute.
    def remember 
      self.remember_token = User.new_token
      update_attribute(:remember_digest,User.digest(remember_token))
    end
    
    def forget
      update_attribute(:remember_digest, nil)
    end
    
    # Set `activated` attribute to true
    def activate_user
      self.update_attribute(:activated, true)
      self.update_attribute(:activated_at, Time.zone.now)
    end
    
    # Send activation email 
    def send_activation_email
      UserMailer.account_activation(self).deliver_now 
    end
    
    # Determines if `remember_token` stored in browser cookie matches `user.remember_digest`.
    # Or if `activation_token` matches activation_digest.
    def authenticated?(attribute, token)
      digest = self.send("#{attribute}_digest")
      return false if digest.nil?
      BCrypt::Password.new(digest) == (token) # `is_password?` is an alias for `==`
    end
    
    # Create password reset digest 
    def create_password_reset_digest 
      self.password_reset_token = User.new_token
      self.update_attribute(:password_reset_digest,User.digest(password_reset_token))
      self.update_attribute(:password_reset_at, Time.zone.now)
    end
    
    # Send password reset email
    def send_password_reset_email
      UserMailer.password_reset(self).deliver_now 
    end
    
    def password_reset_token_expired? 
      self.password_reset_at < 2.hours.ago 
    end
    private 
    
    def email_downcase
       self.email = email.downcase 
    end
    
    # Create activation token 
    def create_activation_code 
      self.activation_token = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end
