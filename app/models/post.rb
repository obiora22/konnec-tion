class Post < ActiveRecord::Base
  belongs_to :user
  default_scope ->{ order(created_at: :desc)}
  validates :user , presence: true 
  validates :content, length: {maximum: 140}
  validates :content, presence: true 
  
  validate :picture_size 
  # Mount uploader 
  mount_uploader :picture, PictureUploader
  
  
  private 
  
  def picture_size 
    if self.picture.size > 5.megabytes 
      self.errors.add(:picture, "File should be less than 5MB")
    end
  end
end
