if Rails.env.production?
   CarrierWave.configure do |config|
       config.fog_credentials = {
        #   Configuration for AWS S3
        :provider => 'AWS',
        :aws_access_key_id => ENV['s3_access_key'],
        :aws_secret_access_key => ENV['s3_secret_key']
       }
       config.fog_directory = ENV['s3_bucket']
   end
end