# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(
    name: 'mungo park',
    email: 'mungo@example.com',
    password: 'password',
    password_confirmation: 'password',
    admin: true,
    activated: true,
    activated_at: Time.zone.now 
    )
# create users 
100.times do |n| 
   name = Faker::Name.name 
   email = "user#{n}@example.com"
   password = 'foobar'
   User.create!(name: name, email: email, password: password, password_confirmation: password,activated: true,activated_at: Time.zone.now )
end

users = User.order(:created_at).take(6)

# create posts
50.times do
   content = Faker::Lorem.sentence(5)
   users.each do |user| 
      user.posts.create!(content: content)
   end
end

# create following relationships 
users = User.all 
user = users.first 
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed)}
followers.each { |follower| follower.follow(user)}