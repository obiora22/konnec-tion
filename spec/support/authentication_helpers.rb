require File.expand_path('../../../app/helpers/sessions_helper', __FILE__)
module AuthenticationHelpers 
   def log_in_as(user) 
      visit '/'
      click_link 'Login'
      fill_in 'Email', with: user.email 
      fill_in 'Password', with: user.password 
      click_button 'Log In'
      expect(page).to have_content("You've logged in succsessfully!")
   end
end
module SignInHelpers
   def sign_in(user,option = {})
     session[:user_id] = user.id 
     cookies[:user_id] = user.id if option[:remember_me] == '1'
   end
end  

   RSpec.configure do |c| 
       c.include AuthenticationHelpers, type: :feature 
   end
   RSpec.configure do |c| 
     c.include SignInHelpers, type: :view
   end
   RSpec.configure do |c|
     c.include SessionsHelper
   end
   
   RSpec.configure do |c| 
     c.include SignInHelpers, type: :controller
   end
