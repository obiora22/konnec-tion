require 'rails_helper'

RSpec.describe User, type: :model do
    
       it 'is invalid if name is empty' do 
        user = User.new(name: '  ')
        user.valid?
       end
       
       it 'is invalid if email is empty' do 
          user = User.new(name: 'Mungo',email: '') 
          expect(user).to_not be_valid
       end
       
       it 'is invalid if name has more than 10 characters' do 
          user = User.new(name: 'M'*51 ,email: 'mungo@example.com') 
          expect(user).to_not be_valid
       end
       
       it 'is invalid if email format does not match required format' do 
          user = User.new(name: 'Mungo', email: 'mungo@example@.com')
          user.save 
          expect(user).to_not be_valid 
       end
       
       it 'is invalid if email is not unique' do 
           user_one = User.create(name: 'Mungo',email: 'unique@example.com')
           user_two = user_one.dup # Test for case sensitivity 
           user_two.email.upcase!
           user_two.save
           expect(user_two).to_not be_valid 
           User.delete(user_one.id)
       end
       
       it 'is VALID if all attributes meet validation requirments' do 
          user = User.new(name: 'Mungo', email: 'mungo@example.com',password: '123456',password_confirmation: '123456') 
          expect(user).to be_valid 
       end
       
       context 'Activation token' do 
          let!(:user){ create(:user)}
          it 'is generated for new user' do 
              expect(user.authenticated?(:activation,user.activation_token)).to eq(true)
          end
       end
       
       context 'follow and unfollow a user' do 
          let!(:user){ create(:user)} 
          let!(:other_user){ create(:user)} 
          it 'follows a user' do 
            user.follow(other_user)
            expect(user.follows?(other_user)).to eq(true)
          end
          it'is followed by a user' do 
            user.follow(other_user)
            expect(other_user.followers.include?(user)).to eq(true)
          end
          
       end
       context 'post feed' do 
         let!(:user_one){ create(:user)} 
         let!(:user_two){ create(:user)} 
         let!(:user_three){ create(:user)}
         before do 
           create(:post,user_id: user_one.id)
           create(:post,user_id: user_two.id)
           create(:post,user_id: user_three.id)
         end
         it "should contain posts from followed user" do 
           user_one.follow(user_two)
           user_two.posts.each do |post_following|
             expect(user_one.feed.include?(post_following)).to eq(true)
           end
         end
         it "should contain posts from self" do 
           user_one.posts.each do |self_post| 
             expect(user_one.feed.include?(self_post)).to eq(true)
           end
           
         end
         it "shouldn't contain posts from an unfollowed user" do 
           user_three.posts.each do |post_not_following|
            expect(user_one.feed.include?(post_not_following)).to eq(false)
           end
          end
        end
end
