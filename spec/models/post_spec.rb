require 'rails_helper'

RSpec.describe Post, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  context 'post association' do 
    # before do 
    #     @user = create(:user)
    # end
    let!(:user){ create(:user)}
    let!(:other_user){ create(:user)}
    let!(:post1){ create(:post, user: user)}
    let!(:post2){ create(:post, user: other_user)}
    it 'is invalid without associated user' do 
      @post = Post.new(content: 'This is my first post.')
      expect(@post).to_not be_valid
    end
    it 'is valid with associated user' do 
       @post = user.posts.build(content: 'This is my second post.') 
       expect(@post).to be_valid 
    end
    it 'is invalid if content length is more than 140' do 
       content = 'a' * 141 
       @post = Post.new(content: content, user: user) 
       expect(@post).to_not be_valid 
    end
    it 'deletes associated post when user is deleted' do 
        User.first.destroy
      
        expect(Post.count).to eq(1)
    end 
  end
  
  context 'posts retrieval' do 
    let(:user){ create(:user)}
    before do 
       4.times{ create(:post, user: user)} 
       @post = create(:post, user: user)
    end
    it 'retrieves posts in reverse order' do 
       expect(Post.first).to eq(@post)
    end
  end
end
