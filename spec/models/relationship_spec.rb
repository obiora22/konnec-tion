require 'rails_helper'

RSpec.describe Relationship, type: :model do
  
    
  before do 
    @relationship = Relationship.new(follower_id: 1, followed_id: 1)
  end
  it 'is invalid' do 
    @relationship.followed_id = nil 
    expect(@relationship.invalid?).to eq(true)
  end
  
  it 'is valid' do 
    expect(@relationship.invalid?).to eq(false)
  end
  
  
  
 
end
