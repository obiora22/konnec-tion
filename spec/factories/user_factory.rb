FactoryGirl.define do 
   sequence(:email){ |n|  "user#{n}@example.com" }
   factory :user do 
      name 'Mungo'
      email {generate(:email)}
      password 'password'
      password_confirmation 'password'
      activated true 
      activated_at Time.zone.now 
      # Allows you create multiple factories for the same class without repeating attributes.
      factory :admin_user do 
         admin true 
      end
      
      factory :user_with_posts do 
         
         transient do 
            posts_count 5 
         end
         
         after(:create) do |user,evaluator| 
            create_list(:post,evaluator.posts_count, user: user)
         end
      end
   end
   
   # 30.times do |n| 
   #    factory :user do 
   #       name "user#{n}"
   #       email "user#{n}@example.com"
   #       password 'foobar'
   #       password_confirmation 'foobar'
   #    end
   # end
end