require 'rails_helper' 

describe 'users/index.html.erb' do 
    let(:user){ create(:user)}
    let(:admin_user){ create(:admin_user)}
    
    
    context 'DELETE link' do 
       it 'is not displayed if current user is not admin' do 
           sign_in(user)
           
           assign(:users, User.paginate(page: '1'))
           # assign(:users, double('User',paginate: [user,admin_user]))
           # assign(:test, User.paginate(page: '1')) 
           render 
           expect(rendered).to_not  have_link 'Delete'
       end
       
       it 'is displayed if current user is admin' do 
          sign_in(admin_user)
          assign(:users, User.paginate(page: '1'))
         
          render :partial => 'users/user', :locals => {user: user}
          expect(rendered).to have_link 'Delete', href: '/users/2'
       end
    end
    
end