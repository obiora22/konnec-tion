require 'rails_helper' 

describe 'users/show.html.erb' do 
 
  let!(:user){create(:user)}
  let!(:posts){create(:user_with_posts).posts }
   
  it 'shows user' do 
    sign_in(user)
    # assign(:user, double('User', name: 'Mike', email: 'mike@example.com', post: 'I am post.'))
    assign(:user, user)
    assign(:posts, posts)
    render 
   expect(rendered).to have_content(user.name)
  end
   
  context 'post association' do 
    # let!(:user){create(:user)}
    # let!(:posts){create(:user_with_posts).posts }
    let!(:user){ create(:user)}
    let!(:post1){ create(:post, user: user)} 
    let!(:post2){ create(:post, user: user)} 
    before do 
      sign_in(user)
    end
    it 'shows user posts' do 
      assign(:user, user)
      assign(:posts, user.posts.paginate(page: '1'))
    
      render :template => 'users/show'
      # render :partial => 'posts/post', locals: {:post => posts[0]}
      expect(rendered).to have_content(posts[0].content)
     end
   end
  
end