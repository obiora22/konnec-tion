require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do
    let!(:user){ create(:user)}
    let!(:other_user){ create(:user)}
    before do 
      sign_in(user)
    end
  it 'redirects when create action is accessed without logging in' do 
     post :create, followed_id: other_user.id
     expect(response).to redirect_to user_path(other_user)
  end
  it 'follows a user' do 
     xhr :post, :create, followed_id: other_user.id 
     expect(response.status).to eq(200)
  end
  it 'unfollows a user' do 
     xhr :post, :create, followed_id: other_user.id 
     expect(response.status).to eq(200)
     relationship = user.active_relationships.find_by(followed_id: other_user.id)
     xhr :delete, :destroy, id: relationship.id 
     expect(response.status).to eq(200)
  end
end
