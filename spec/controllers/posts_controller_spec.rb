require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  let!(:user1){ create(:user)}
  let!(:post1){ create(:post, content: 'First post.',user_id: user1.id)}
  let!(:user2){ create(:user)}
  let!(:post2){ create(:post, content: 'Second post',user_id: user2.id)}
  
  context 'create' do 
    it 'redirects when not logged in' do 
      post :create, {post: {content: 'I am a post.',user_id: user1.id}}
      expect(response).to redirect_to login_url 
    end
  end
  
  context 'destroy' do 
    before do 
      sign_in(user1)
    end
    it 'redirects when you try to delete other\'s post ' do 
      delete :destroy, id: post2.id 
      expect(response).to redirect_to root_url 
    end
  end
end
