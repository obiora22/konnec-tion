require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  User.delete_all 
  let!(:user){ create(:user)}
  let(:non_activated_user){ create(:user, activated: false)}
  context 'when password is invalid' do 
    
     it 'renders page with error message' do 
        post :create, session: {email: 'user@example.com', password: '123456'} 
        expect(response).to render_template(:new)
        expect(flash[:danger]).to eq("Incorrect email/password combination.")
     end
  end
  
  context 'when passoword is valid' do 
     it 'redirects users to their profile page' do 
        post :create, session: {email: user.email, password: user.password}
        expect(response).to redirect_to user_path(user)
        expect(flash[:notice]).to eq("You've logged in succsessfully!")
     end
     it 'redirects when user is not-activated ' do 
       post :create, session: {email: non_activated_user.email, password: non_activated_user.password}
       expect(response).to redirect_to root_url 
       message = "Account not activated. "
       message+= "Please check your email for the activation link."
       expect(flash[:warning]).to eq(message)
     end
  end
  
  context 'valid credentials with persistent session' do 
     it 'remembers user' do 
        post :create, session: {email: user.email, password: user.password, remember_me: '1'}
        expect(response.cookies['remember_token']).to_not eq(nil)
      end
  end
 
  
end
