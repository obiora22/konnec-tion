require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  
  
  let!(:other_user){ create(:user, name: 'Mike', email: 'mike@example.com')}
  let!(:user){create(:user, admin: false)}
  let!(:admin_user){ create(:admin_user)}
  

  
  context 'Edit' do
    it 'redirects edit when not logged in' do
      get :edit, id: user.id
      expect(flash[:alert]).to eq("You have to be logged in.")
      expect(response).to redirect_to login_url 
    end
  end
  context 'Update' do 
    it 'Redirects update when not logged in' do 
      patch :update, {user: {name: 'Mungo Parker', email: user.email},id: user.id}
      expect(flash[:alert]).to eq("You have to be logged in.")
      expect(response).to redirect_to login_url 
    end
  end
  context 'Edit right user' do 
    it 'redirects update when logged in as wrong user' do 
      sign_in(user,{remember_me: '0'})
      patch :update, user: {name: other_user.name, email: other_user.email}, id: other_user.id 
      expect(response).to redirect_to root_url 
    end
  end
  
  context 'Index' do 
    it 'redirects when not logged in' do 
      get :index 
      expect(response).to redirect_to login_url 
    end
    
  end
  
  context 'Delete' do 
    it 'deletes user successfully if current user is admin' do 
      sign_in(admin_user)
      delete :destroy, id: user.id 
      expect(User.count).to eq(2)
    end
    
    it 'redirects if current user is not admin' do 
      sign_in(user)
      delete :destroy, id: admin_user.id 
      expect(response).to redirect_to root_url 
    end
  end
  context 'user relationship' do 
    
    it 'returns followers' do 
      get :followers, id: user.id
      expect(response).to redirect_to login_url 
    end
    it 'returns following' do 
      get :following, id: user.id 
      expect(response).to redirect_to login_url
    end
  end
end
