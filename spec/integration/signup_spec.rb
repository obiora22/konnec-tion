require 'rails_helper' 

feature 'Sign up' do 
   before do
      
      visit '/' 
   end
   scenario 'Successful sign up' do 
       name = 'Mungo Park'
       email = 'mungo@example.com'
       password = 'password'
       password_confirmation = 'password'
       click_link 'Sign Up'
       fill_in 'Name', with: name 
       fill_in 'Email', with: email 
       fill_in 'Password', with: password 
       fill_in 'Password confirmation', with: password 
       click_button 'Create Account' 
       expect(page).to have_content("Please check your email for to activate your account.")
       
   end
   
   scenario 'Unsuccessful sign up' do 
      name = 'Mungo Park'
      wrong_email = 'mungo@example@@.com'
      password = 'password'
      password_confirmation = 'password' 
      click_link 'Sign Up'
      fill_in 'Name', with: name 
      fill_in 'Email', with: wrong_email 
      fill_in 'Password', with: password 
      fill_in 'Password confirmation', with: password 
      click_button 'Create Account' 
      expect(page).to have_content('This form contains 1 error')
     
   end
   scenario 'Sign up automatically generates activation email for user' do 
    ActionMailer::Base.deliveries.clear 
    visit '/'
    click_link 'Sign Up'
    fill_in 'Name', with: 'Mungo Park'
    fill_in 'Email', with: 'mungo@example.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'
    click_button 'Create Account'
    # check email delivery with email_spec
    expect(ActionMailer::Base.deliveries.size).to eq(1)
    expect(page).to have_content("Please check your email for to activate your account.")
  end
end