require 'rails_helper' 

feature 'password reset' do 
   let!(:user){ create(:user)}
   scenario 'is successful' do 
     visit '/'
     click_link 'Login'
     click_link 'Forgot password?'
     # email form 
     fill_in 'Email', with: user.email 
     click_button 'Submit'
     # send password reset link
     expect(page).to have_content('Email sent with password reset link')
   end
end