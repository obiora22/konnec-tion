require 'rails_helper' 

feature 'create posts' do 
    let!(:user){ create(:user)}
    before do 
       log_in_as(user) 
       visit root_url 
    end
   scenario 'create posts successfully' do 
       fill_in 'Content', with: 'I am the very first post.'
       click_button 'Post'
       expect(page).to have_content('Post created.')
   end
end