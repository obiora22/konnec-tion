require 'rails_helper' 

feature 'delete post' do 
  let!(:user){ create(:user)}
  let!(:post1){ create(:post, user: user)} 
  let!(:post2){ create(:post, user: user)} 
  # let!(:post){ create(:post)}
  # let!(:posts){create(:user_with_posts).posts}
  before do 
   log_in_as(user) 
   visit root_url 
  end
   scenario 'delete' do 
 
     within("li#post-#{post1.id}") do 
       click_link 'Delete'
     end
     expect(page).to have_content('Post deleted.')
   end
end