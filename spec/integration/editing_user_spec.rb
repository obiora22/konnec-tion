require 'rails_helper'

feature 'Edit user' do 
   #let!(:user){ create(:user)} 
   before(:each) do 
      @user = create(:user) 
   end
   before do 
    log_in_as(@user)
    visit user_path(@user)
    click_link 'Edit User'
   end
   
   def fill_in_password_and_update
     fill_in 'Password', with: @user.password
     fill_in 'Password confirmation', with: @user.password
     click_button 'Update User'
   end
   scenario 'Edit a user successfully' do 
     fill_in 'Name', with: 'Mungo Mike'
     fill_in_password_and_update
     expect(page).to have_content("Your profile has been updated.")
   end
   
   scenario 'Edit a user unsuccessfully' do 
      fill_in 'Name', with: ' '
      fill_in_password_and_update
      expect(page).to have_content("Something went wrong!")
   end
end