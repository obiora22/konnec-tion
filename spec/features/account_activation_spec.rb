require 'rails_helper' 

feature 'account activation email' do 
   
 
  
   scenario 'Sign up automatically generates activation email for user' do 
    visit '/'
    click_link 'Sign Up'
    fill_in 'Name', with: 'Mungo Park'
    fill_in 'Email', with: 'mungo@example.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'
    click_button 'Create Account'
    # check email delivery with email_spec
    expect(page).to have_content("Please check your email for to activate your account.")
  end
end