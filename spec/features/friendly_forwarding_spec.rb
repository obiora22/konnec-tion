require 'rails_helper' 

feature 'Friendly forwarding' do 
    !let(:user){ create(:user)}
    scenario 'forward user to profile edit page after initial redirect to login page' do 
        visit edit_user_path(user)
        expect(page).to have_text("You have to be logged in.")
        log_in_as(user)
        expect(page).to have_content('Edit Profile')
    end
end