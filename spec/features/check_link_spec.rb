require 'rails_helper'

feature 'clicking links' do 
    let!(:user){create(:user)}
    scenario 'click home link' do 
       visit '/home'
       click_link('Home') 
       expect(page).to have_link 'Login'
    end
    
    scenario 'Right links for signed in user' do 
       log_in_as(user)
       expect(page).to_not have_content('Log In')
    end
    
end