require 'rails_helper' 

feature 'Log out' do 
   !let(:user){ create(:user) }
   before do
      log_in_as(user) 
   end
   scenario 'Successful log out ' do 
     click_link 'Logout'
     expect(page).to have_content("You've been succsessfully logged out.")
   end
end