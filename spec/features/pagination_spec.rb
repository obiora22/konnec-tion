require 'rails_helper'

feature 'Users pagination' do 
   
   before do 
      @admin_user = create(:admin_user)
      20.times{create(:user)}
      log_in_as(@admin_user)
   end
  
   it 'paginates users index page' do 
       visit users_path
       expect(page).to have_content('Users')
       expect(User.paginate(page: 1, per_page: 15).size).to eq(15)
   end
end